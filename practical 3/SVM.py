from sklearn import svm
import json
import pickle
import os.path
import numpy as np

layers = ['conv5', 'relu5', 'pool5', 'linear6', 'relu6', 'linear7', 'relu7', 'linear8']
dataFolder = 'data/'

#torch2py('testFeatures-linear8')

def torchload(filename):
	import torchfile
	return torchfile.load(filename + '.t7')

def torch2py(filename):
	print 'Torch2py: ' + filename, ' .',

	data = torchload(filename)
	print '.',

	#convert numpy types to list in order for JSON to work
	data = {k: v.tolist() if type(data.label).__module__ == 'numpy' else v for k,v in data.iteritems() }
	
	with open(filename + '.json', 'w') as outfile:
		json.dump(data, outfile)

	print '.'	

def convertFeatures():
	'''
	Converts all torch featuremaps to JSON files
	'''
	for dataSet in ['test', 'train']:
		for layer in layers:
			torch2py(dataFolder + dataSet + 'Features-' + layer)

def SVM(layer):
	data = loadFeatures(layer)

	X = data['train']['data']
	Y = data['train']['label']

	scores = {}
	lin_clf = svm.LinearSVC()
	print 'Training SVM %s . . .' % layer
	lin_clf.fit(X, Y)

	print 'Evaluation SVM %s . . .' % layer
	scores['train'] = lin_clf.score(X, Y)
	scores['test']  = lin_clf.score(data['test']['data'], data['test']['label'])

	return scores

def loadFeatures(layer):
	data = {}
	for dataSet in ['test', 'train']:
		baseFile = dataFolder + dataSet + 'Features-' + layer
		#if not os.path.isfile(baseFile + '.json'):
		#	torch2py(baseFile)
		#with open(baseFile + '.json') as f:
		#	d = json.load(f)
		d = torchload(baseFile)
		data[dataSet] = d
	return data

def batch_score_SVMs(layers):

	scores = {}
	for layer in layers:
		storeFile = dataFolder + 'SVM-' + layer + 'scores.json'

		# This SVM has not yet been trained
		if not os.path.isfile(storeFile):
			#open the file, to create it and prevent parallel scripts from testing the same SVM
			with open(storeFile, 'a') as outfile:
				json.dump(SVM(layer), outfile)

def loadScores(layers):
	#scores = {layer: {'train': np.random.uniform(), 'test':np.random.uniform()} for layer in layers}
	scores = {}
	for layer in layers:
		with open('data/SVM-%sscores.json' % layer) as f:
			s = json.load(f)
		scores[layer] = s

	return scores
	
def plotScores(scores):
	import matplotlib.pyplot as plt

	trainScores = [scores[layer]['train'] for layer in layers]
	testScores  = [scores[layer]['test'] for layer in layers]

	ind = np.arange(len(layers))  # the x locations for the groups
	width = 0.35       # the width of the bars

	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, trainScores, width, color='green')

	testMeans = (25, 32, 34, 20, 25, 23, 3, 35)
	rects2 = ax.bar(ind + width, testScores, width, color='lightgreen')

	# add some text for labels, title and axes ticks
	ax.set_ylabel('Mean accuracy (proportional)')
	ax.set_ylim([0, 1])
	ax.set_title('SVM Scores using different features')
	ax.set_xticks(ind + width)
	ax.set_xticklabels(layers)

	ax.legend((rects1[0], rects2[0]), ('[Train]', '[Test]'))





def main():
	import matplotlib.pyplot as plt
	#batch_score_SVMs(layers)\
	scores = loadScores(layers)
	print scores
	plotScores(scores)
	plt.savefig('images/SVM_scores.png')


if __name__ == '__main__':
	main()