require 'nn'

local function alexnet_model(num_output_classes, batch_normalize)

    local batch_normalize = batch_normalize or false
    local num_output_classes = num_output_classes or 10

   
    local alexnet = nn.Sequential()

    -- WE GIVE YOU THE FIRST MODULE FOR THIS ALEXNET-LIKE FRAMEWORK
    -- YOU NEED TO IMPLEMENT THE REST OF THE NETWORK
    -- NOT ALL MODULES HAVE TRAINABLE PARAMETERS
    -- ADD THE w_lr (weight learning rate) and b_lr (bias learning rate)
    -- ONLY TO THE MODULES THAT HAVE TRAINABLE PARAMETERS

    -- IN MANY IMPLEMENTATIONS YOU WILL SEE THAT ONE STRICTLY ONLY NEEDS TO 
    -- DEFINE THE MODULE AND ITS INITIAL PARAMETERS
    
    -- IN MANY IMPLEMENTATIONS YOU WILL SEE THAT ONE STRICTLY ONLY NEEDS TO 
    -- DEFINE THE MODULE AND ITS INITIAL PARAMETERS. FOR OUR CONVENIENCE WE
    -- CAN INCLUDE FURTHER HELPER VARIABLES THAT PERHAPS WE ARE GOING TO USE NEXT
    
    -- ONE SUCH HELPER VARIABLE IS AN ID/NAME FOR THE MODULE.
    -- WE CAN ASSIGN DIFFERENT NAMES TO MODULES, SO THAT WE CAN DO OUR
    -- ANALYSIS OR DEBUGGING EASIER. E.G. WE MIGHT WANT TO EXCLUDE A SPECIFIC
    -- LAYER WHEN INITIALIZING A NEW LAYER WITH THE WEIGHTS OF AN EXISTING,
    -- PRETRAINED NETWORK

    -- ANOTHER SUCH VARIABLE ARE MODULE LEARNING RATES. IN TORCH YOU CANNOT
    -- EXPLICITLY DEFINE A LEARNING RATE PER MODULE. INSTEAD, YOU CAN DEFINE
    -- A LEARNING RATE VECTOR FOR THE WHOLE VECTOR, WHERE EACH DIMENSION REFERS
    -- TO EACH OF THE PARAMETERS OF THE NETWORK (AS OBTAINED FROM THE FUNCTION 
    -- params, grad_params = model:getParameters()). HENCE, HERE WE CAN DEFINE
    -- A VARIABLE WITH THE INITIAL LEARNING RATE OF THE MODULE. THEN THE LEARNING
    -- RATE SCHEDULE OF THE OPTIMIZATION FUNCTION TAKES CARE OF THE REST.

    -- YOU CAN ADD MORE MODULE HELPER VARIABLES, IF THEY HELP YOU WITH THE 
    -- NETWORK DEFINITION AND TRAINING

    local stride = 1

    mod = nn.SpatialConvolution(3, 96, 5, 5, stride, stride, 2, 2)
    mod.id   = 'conv1'
    alexnet:add(mod)


    mod = nn.ReLU()
    mod.id   = 'relu1'
    alexnet:add(mod)

    mod = nn.SpatialMaxPooling(2, 2)
    mod.id = 'pool1'
    alexnet:add(mod)

    mod = nn.SpatialConvolution(96, 256, 5, 5, stride, stride, 2, 2)
    mod.id   = 'conv2'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.SpatialBatchNormalization(256)
        mod.id   = 'batchnorm2'
        alexnet:add(mod)        
    end

    mod = nn.ReLU()
    mod.id   = 'relu2'
    alexnet:add(mod)

    mod = nn.SpatialMaxPooling(2, 2)
    mod.id = 'pool2'
    alexnet:add(mod)

    mod = nn.SpatialConvolution(256, 384, 3, 3, stride, stride, 1, 1)
    mod.id   = 'conv3'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.SpatialBatchNormalization(384)
        mod.id   = 'batchnorm3'
        alexnet:add(mod)        
    end

    mod = nn.ReLU()
    mod.id   = 'relu3'
    alexnet:add(mod)

    mod = nn.SpatialConvolution(384, 384, 3, 3, stride, stride, 1, 1)
    mod.id   = 'conv4'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.SpatialBatchNormalization(384)
        mod.id   = 'batchnorm4'
        alexnet:add(mod)        
    end

    mod = nn.ReLU()
    mod.id   = 'relu4'
    alexnet:add(mod)   

    mod = nn.SpatialConvolution(384, 256, 3, 3, stride, stride, 1, 1)
    mod.id   = 'conv5'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.SpatialBatchNormalization(256)
        mod.id   = 'batchnorm5'
        alexnet:add(mod)        
    end    

    mod = nn.ReLU()
    mod.id   = 'relu5'
    alexnet:add(mod)

    mod = nn.SpatialMaxPooling(2, 2)
    mod.id = 'pool5'
    alexnet:add(mod)

    -- Flatten convolutional net into a flat nn (losing all dimensions).
    -- (0.5^3) because we do max-pooling 3 times, reducing the data by a factor of 2
    local D = 256 * (1 / stride) * trainData.data:size()[3] * (0.5^3) * trainData.data:size()[4] * (0.5^3)

    mod = nn.Reshape(D)
    mod.id = 'view5'
    alexnet:add(mod)

    mod = nn.Dropout(0.5)
    mod.id = 'dropout6'
    alexnet:add(mod)

    mod = nn.Linear(D, 4096)
    mod.id = 'linear6'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.BatchNormalization(4096)
        mod.id   = 'batchnorm6'
        alexnet:add(mod)        
    end      

    mod = nn.ReLU()
    mod.id = 'relu6'
    alexnet:add(mod)

    mod = nn.Dropout(0.5)
    mod.id = 'dropout7'
    alexnet:add(mod)

    mod = nn.Linear(4096, 1024)
    mod.id = 'linear7'
    alexnet:add(mod)

    if batch_normalize then
        mod = nn.BatchNormalization(1024)
        mod.id   = 'batchnorm7'
        alexnet:add(mod)        
    end      

    mod = nn.ReLU()
    mod.id = 'relu7'
    alexnet:add(mod)

    mod = nn.Dropout(0.5)
    mod.id = 'dropout8'
    alexnet:add(mod)  

    mod = nn.Linear(1024, num_output_classes)
    mod.id = 'linear8'
    mod.w_lr = 1.0
    mod.b_lr = 1.0
    alexnet:add(mod)

    return alexnet

end

function define_alexnet_model_cifar(num_output_classes, batch_normalize)

    if usingCUDA then
        model = nn.Sequential()
        -- CONVERT THE MODEL SO THAT IT WORKS WITH CUDA
        model:add(nn.Copy('torch.FloatTensor','torch.CudaTensor'):cuda())
        model:add(alexnet_model(num_output_classes, batch_normalize):cuda())
        return model
    end
    return alexnet_model(num_output_classes, batch_normalize)    
end