#!/bin/bash
#SBATCH -p gpu
#SBATCH -N 1
#SBATCH -t 5-00:00
module load cuda
module load torch7
module load python
cd ~/python-torchfile/
python setup.py install --user 
cd ~/practicals-dl/practical\ 3
srun -u python "SVM.py"
