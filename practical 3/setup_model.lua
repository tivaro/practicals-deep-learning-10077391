
-- INITIALIZE WEIGHTS RANDOMLY FOLLOWING THE MSRA (MICROSOFT RESEARCH ASIA STYLE)
function initializeNetRandomly(net)
    local function initializeLayer(name)
        for k,v in pairs(net:findModules(name)) do
            local dims  = v.weight:size()
            local sigma = 1
            local mu = 0

            -- INITIALIZE THE WEIGHTS THEM WITH THE RESNET-LIKE INITIALIZTION.
            if name == 'nn.SpatialConvolution' then
                -- SIGMA0 = KERNEL_WIDTH * KERNEL_HEIGHT * NUM_OUTPUT_DIMS
                -- SIMGA  = SQUARE ROOT OF (2/SIGMA0)
                sigma = dims[3] * dims[4] * dims[1]
                sigma = math.sqrt(2 / sigma)
            end

            -- For now, we just use Xavier initialisations
            if name == 'nn.Linear' then
                -- SIGMA = SQUARE ROOT OF (1 / NUM_INPUT_DIMS)
                sigma = math.sqrt(1 / dims[2])
            end

            -- sample weights from normal dist and set biases to 0
            v.weight = torch.randn(dims):mul(sigma):add(mu)
            v.bias:fill(0)
        end
    end

    -- GO THROUGH ALL THE LAYERS OF THE NETWORK
    -- FIND THE ONES THAT CONTAIN TRAINABLE PARAMETERS
    initializeLayer('nn.Linear')
    initializeLayer('nn.SpatialConvolution')

    return net
end

function add_batch_normalisation(model, parameterized)
    -- add batch_normalisation modules to all convolutional and linear layers (exept last one)

    local offset  = 0
    local net = model
    local modules = net:listModules()
    local parameterized = parameterized or false

    for k,v in pairs(modules) do

        --not for the last module
        if k < #modules then

            local typename = torch.typename(v)
            local mod

            -- if we found a nested layer, enter this layer and set the offset to this layer (-k
            -- we add 1 to the offset, because we want to add the batch_norm AFTER the current layer
            if typename == 'nn.Sequential' then
                net = modules[k]
                offset = -k + 1
            end

            if typename == 'nn.SpatialConvolution' then
                mod = nn.SpatialBatchNormalization(v.nOutputPlane, nil, nil, false)
            end
            if typename == 'nn.Linear' then
                mod = nn.BatchNormalization(v.bias:size()[1], nil, nil, false)
            end

            if mod then
                -- cast to cuda if nescecary
                if usingCUDA then mod:cuda() end

                net:insert(mod, k + offset)
                offset = offset + 1
            end
        end

    end


    return model

end


-- INITIALIZE WEIGHTS FROM AN EXISTING NETWORK
-- THIS IS ALSO CALLED TRANSFER LEARNING, WHERE THE EXISTING LEARNING
-- FROM ANOTHER DATASET TO THE NEW ONE
function initializeNetWithNet(model, fromfile)

    -- IN THIS FILE WE ASSUME THAT THE PRETRAINED AND THE FINAL MODULE HAVE 
    -- A SIMILAR ARCHITECTURE, NAMELY n_C NUMBER OF CONVOLUTIONAL LAYERS AND 
    -- N_F NUMBER OF FULLY CONNECTED LAYERS

    -- LOAD THE PRETRAINED MODEL FROM THE fromfile PATH
    local init_model = torch.load(opt['save'] .. fromfile .. '.net')

    -- First, initialise the net randomly, next, overwrite the layers that exist in both nets 
    model = initializeNetRandomly(model)

    -- RETRIEVE THE  WEIGHTS AND BIASES FROM THE PRETRAINED MODEL FROM
    -- THE MODULES THAT CONTAIN TRAINABLE PARAMETERS
    -- SET THE NEW MODEL MODULES WITH THE RESPECTIVE WEIGHTS
    
    -- START WITH THE SPATIAL CONVOLUTION MODULES
    init_modules = init_model:findModules('nn.SpatialConvolution')
    modules      = model:findModules('nn.SpatialConvolution')

    for i, md in pairs(modules) do
        md.weight = init_modules[i].weight:clone()
        md.bias   = init_modules[i].bias:clone()

        print('Layer ' .. md.id .. ':')
        print(' > Norm of differences:' ..
              ' Weight=' .. (init_modules[i].weight - modules[i].weight):norm(2) ..
              ' Bias=' ..   (init_modules[i].bias   - modules[i].bias):norm(2) )
        print(' > Norm: Weight=' .. md.weight:norm(2) .. ' Bias=' .. md.bias:norm(2))
    end

    -- REPEAT FOR THE FULLY CONNECTED MODULES.
    init_modules = init_model:findModules('nn.Linear')
    modules      = model:findModules('nn.Linear')

    for i, md in pairs(modules) do
        -- THE LAST LAYER THAT DELIFERS THE CLASSIFICATION IS DIFFERENT,
        -- SINCE THE DATASETS ARE DIFFERENT. IGNORE THIS LAYER FROM THE PRETRAINED
        -- NETWORK. 
        print('Layer ' .. md.id .. ':')

        if md.id ~= 'linear8' then
            md.weight = init_modules[i].weight:clone()
            md.bias   = init_modules[i].bias:clone()

            print(' > Norm of differences:' ..
                  ' Weight=' .. (init_modules[i].weight - modules[i].weight):norm(2) ..
                  ' Bias=' ..   (init_modules[i].bias   - modules[i].bias):norm(2) )
        end

        print(' > Norm: Weight=' .. md.weight:norm(2) .. ' Bias=' .. md.bias:norm(2))
    end

    -- cast to CUDA if nescecariy
    if usingCUDA then model:cuda() end
    
    return model
end


-- SETUP DIFFERENT LEARNING RATES PER LAYER. THIS IS RELEVANT WHEN THE DIFFERENT
-- LAYERS ARE AT A DIFFERENT STATE OF OPTIMIZATION (E.G. WHEN LOADING A PRETRAINED
-- NETWORK AND ADDING ON TOP A COMPLETELY NEW MODULE)
function setupLearningRateVector(model, model_opt)
    local modules  = model:listModules()

    local weight_sizes = {} -- KEEP THE SIZE OF PARAMETERS PER LAYER
    local bias_sizes   = {} -- KEEP THE SIZE OF BIASES PER LAYER
    local total_size   = 0

    for mi, mod in pairs(modules) do
        -- CACHE THE NUMBER OF TRAINABLE PARAMETERS AND BIASES PER LAYER
        if mod.weight then
            weight_sizes[mi] = mod.weight:numel()
            bias_sizes[mi]   = mod.bias:numel()

            total_size   = total_size + weight_sizes[mi] + bias_sizes[mi]
        end
    end

    print(weight_sizes)
    print(bias_sizes)

    print(total_size)

    assert(model:getParameters():size(1) == total_size, 'Could not find all paramters')


    -- IN total_size YOU STORE THE TOTAL SIZE OF TRAINABLE PARAMETERS, INCLUDING THE BIASES
    lrvector = torch.Tensor(total_size):fill(-1)
    local offset = 1
    for id, _ in pairs(weight_sizes) do
        
        -- HERE YOU USE THE CACHED NUMBER OF TRAINABLE PARAMETES PER LAYER
        wsize = weight_sizes[id]
        bsize = bias_sizes[id] 

        -- fill with layer specific leanring rate if specified, and with default learningRate otherwise
        local w_lr = modules[id].w_lr or model_opt['defaultLearningRate']
        local b_lr = modules[id].b_lr or model_opt['defaultLearningRate']

        -- DIMENSIONS CORRESPONDING TO THE MODULE WEIGHT PARAMETERS
        lrvector[{{offset, offset + wsize - 1}}]:fill(w_lr)
        offset = offset + wsize
        
        -- DIMENSIONS CORRESPONDING TO THE MODULE BIAS PARAMETERS
        lrvector[{{offset, offset + bsize - 1}}]:fill(b_lr)
        offset = offset + bsize
    end


    -- HERE YOU CREATE A NEW FIELD IN THE model_opt structure to include the 
    -- LEARNING RATE VECTOR. THE OPTIMIZERS SEARCH FOR THE VARIABLE learningRates
    -- WHEN ONE DEFINES DIFFERENT LEARNING RATES PER LAYER
    model_opt['learningRates'] = lrvector    
end

function extract_features(model, inputs, layers, batch_size)
    local batch_size = batch_size or 128
    local modules = model:listModules()
    local useLayers = {}
    local features  = {}

    local num_inputs = inputs:size(1)

    -- Select and store the layers that we are going to use, and their index in modules
    for k,md in pairs(modules) do
        for _,id in ipairs(layers) do
            if id == md.id then
                useLayers[k] = id
            end            
        end
    end

    -- Forward a single image to determine the layers output size, and preallocate the tensors
    model:forward(inputs[{{1,}}])
    for k,id in pairs(useLayers) do
        features[id] = torch.FloatTensor(num_inputs, modules[k].output:numel())
    end


    local ti = 1
    local ftotal = 0
    for t = 1, num_inputs, batch_size do

        -- disp progress
        xlua.progress(ti, math.ceil(num_inputs/ batch_size))
        ti = ti + 1 

        -- create mini batch and forward it trough the network
        local batch_start = t
        local batch_end   = math.min(t + batch_size - 1, num_inputs)
        model:forward(inputs[{{batch_start, batch_end}}])

        -- store the activations for the desired layers (flatten and convert to float for storage)
        for k,id in pairs(useLayers) do
            local f = modules[k].output:float()
            f:reshape(f:numel())
            features[id][{{batch_start, batch_end}}] = f
        end
    end    

    print(features)

    return features


end

