require 'torch'

------------------------------------------------------
print '=== I. ENVIRONMENT PARAMETERS ==='
dofile 'setGPUtensor.lua'
torch.setnumthreads(1)
torch.manualSeed(1)

------------------------------------------------------
print '=== II. EXPERIMENT OPTIONS ==='
opt                 = {}
opt['save']         = 'data/'
opt['max_epoch']    = 100

------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt                   = {}
model_opt['batchSize']      = 128
model_opt['learningRate']   = 1.0
model_opt['weightDecay']    = 0.0005
model_opt['momentum']       = 0.9
model_opt['model_name']     = 'alexnet_cifar100B3-1'
model_opt['save_step']      = 100
model_opt['load_net']       = 'alexnet_egavves'

--on every step iterations, the learning rate is divided by the factor
model_opt['learningRate_schedule'] = nil

----------------------------------------------------------------------
-- Section 2.1
----------------------------------------------------------------------
------------------------------------------------------
print '=== A. LOAD DATA ==='
dofile 'load_data_cifar100.lua'
trainData, testData = load_data_cifar100()

------------------------------------------------------
print '=== B. PREPROCESS DATA ==='
dofile 'preprocess_data_cifar.lua'
trainData, testData = preprocess_data_cifar(trainData, testData)

------------------------------------------------------
print '=== C. DEFINE MODEL ==='
dofile 'define_alexnet_model_cifar.lua'
model   = define_alexnet_model_cifar(100)
print(model)

------------------------------------------------------
print '=== D. INITIALIZE MODEL ==='
dofile 'setup_model.lua'
model   = initializeNetWithNet(model, model_opt['load_net'])

------------------------------------------------------
print '=== E. DEFINE TRAINING ==='
dofile 'define_training.lua'
train(model, opt, model_opt, trainData, testData)


------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

model_opt['learningRate']   = .1
model_opt['model_name']     = 'alexnet_cifar100B3-2'

------------------------------------------------------
print '=== C. DEFINE MODEL ==='
dofile 'define_alexnet_model_cifar.lua'
model   = define_alexnet_model_cifar(100)
print(model)

------------------------------------------------------
print '=== D. INITIALIZE MODEL ==='
dofile 'setup_model.lua'
model   = initializeNetWithNet(model, model_opt['load_net'])

------------------------------------------------------
print '=== E. DEFINE TRAINING ==='
dofile 'define_training.lua'
train(model, opt, model_opt, trainData, testData)
