require 'torch'

------------------------------------------------------
print '=== I. ENVIRONMENT PARAMETERS ==='
-- We will need the GPU implementation for this experiment.
-- We define the default tensor to be of CUDA type.
dofile 'setGPUtensor.lua'
torch.setnumthreads(1)
torch.manualSeed(1)

------------------------------------------------------
print '=== II. EXPERIMENT OPTIONS ==='
opt                 = {}
opt['save']         = 'data/'
opt['max_epoch']    = 300

------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt                   = {}
model_opt['batchSize']      = 128
model_opt['learningRate']   = 1.0
model_opt['weightDecay']    = 0.0005
model_opt['momentum']       = 0.9
model_opt['model_name']     = 'alexnet_cifar10A4-1'
model_opt['save_step']      = 100

 --on every step iterations, the learning rate is divided by the factor
model_opt['learningRate_schedule'] = {}
model_opt.learningRate_schedule['step']   = 50
model_opt.learningRate_schedule['factor'] = 10 

------------------------------------------------------
print '=== A. LOAD DATA ==='
dofile 'load_data_cifar10.lua'
trainData, testData = load_data_cifar10()

------------------------------------------------------
print '=== B. PREPROCESS DATA ==='
dofile 'preprocess_data_cifar.lua'
trainData, testData = preprocess_data_cifar(trainData, testData)

------------------------------------------------------
print '=== C. DEFINE MODEL ==='
dofile 'define_alexnet_model_cifar.lua'
--model   = define_alexnet_model_cifar(10)
--print(model)

------------------------------------------------------
print '=== D. INITIALIZE MODEL ==='
dofile 'setup_model.lua'
--model   = initializeNetRandomly(model)

------------------------------------------------------
print '=== E. DEFINE TRAINING ==='
dofile 'define_training.lua'
--train(model, opt, model_opt, trainData, testData)

model = torch.load('data/alexnet_cifar10A4-1.net')
if usingCUDA then model:cuda() end

------------------------------------------------------------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt['model_name']     = 'C-alexnet_cifar10A4-2'
model_opt['epoch_offset']   = 1 * opt['max_epoch']

 --on every step iterations, the learning rate is divided by the factor
model_opt['learningRate_schedule'] = nil

print '=== C. DEFINE MODEL ==='
model   = add_batch_normalisation(model)
print(model)

print '=== E. DEFINE TRAINING ==='
dofile 'define_training.lua'
train(model, opt, model_opt, trainData, testData)

------------------------------------------------------------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt['model_name']     = 'C-alexnet_cifar10A4-3'
model_opt['epoch_offset']   = 2 * opt['max_epoch']

--on every step iterations, the learning rate is divided by the factor
model_opt['learningRate_schedule'] = {}
model_opt.learningRate_schedule['step']   = 50
model_opt.learningRate_schedule['factor'] = 10


print '=== E. DEFINE TRAINING ==='
dofile 'define_training.lua'
train(model, opt, model_opt, trainData, testData)