require 'torch'
require 'optim'
require 'load_csv'

function load_model(model_id, data_folder)
	-- get the paths
	local data_folder = data_folder or 'data/'
	local model_dir = paths.concat(data_folder)

	local model = {['accuracy'] = {}, ['loss'] = {}}
	--load the log data
	local train = load_csv(paths.concat(model_dir, model_id .. '_train.log'))
	local test  = load_csv(paths.concat(model_dir, model_id .. '_test.log'))
	
	-- Process the log data
	model['accuracy']['train'] = train[{{},1}]:squeeze()
	model['accuracy']['test']  = test[{{},1}]:squeeze()

	model['loss']['train'] = train[{{},2}]:squeeze()
	model['loss']['test']  = test[{{},2}]:squeeze()

	-- Create the X Range
	model['epochs'] = model['loss']['train']:numel()
	model['xrange'] = train[{{},3}]:squeeze()

	return model

end

function compare_models(model_ids, plot_loss, legend)

	Plot = require 'itorch.Plot'

	local plot_loss = plot_loss or false
	local legend = legend or {}

	--load the models
	local models = {}
	for i,model_id in ipairs(model_ids) do
		models[i] = load_model(model_id)
	end

	local colors = {'lightgreen','darkgreen','lightblue','darkblue','red','darkred','orange','yellow','purple','pink'}


	local plot = Plot()
	for i,model in ipairs(models) do
		local lbl = (legend[i] or '')
		plot:line(model.xrange, model.accuracy.train, colors[2*i-1], lbl .. ' [train]')
		:line(model.xrange, model.accuracy.test, colors[2*i], lbl .. ' [test]')
	end
	plot:title('Mean Accuracy')
	:yaxis('Mean Accuracy')
	:xaxis('epochs')
	:legend(true)
	:draw()

	if plot_loss then
		plot = Plot()
		local max_loss = 1--math.max(model.loss.train:max(),model.loss.test:max())
		for i,model in ipairs(models) do
			local lbl = (legend[i] or '')
			plot:line(model.xrange, torch.div(model.loss.train, max_loss), colors[2*i-1], lbl .. ' [train]')
			:line(model.xrange, torch.div(model.loss.test, max_loss), colors[2*i], lbl .. ' [test]')
		end
		plot:title('Loss')
		:yaxis('Loss')
		:xaxis('epochs')
		:legend(true)
		:draw()
	end

	-- Display performance in a table
	local maxLblSize = 10
	--[[for i,model in ipairs(models) do
		maxLblSize = math.max(maxLblSize, #model.model_opts.model_lbl)
	end]]--

	--[[
	print(string.format("% " .. maxLblSize .. "s |  Train  | Test     |", 'Model'))
	print(string.format("% " .. maxLblSize .. "s-+---------+----------+", '---------'))
	for i,model in ipairs(models) do
		print(string.format("% " .. maxLblSize .. "s |%3.3f %% | %3.3f %% |", model.model_opts.model_lbl, model.train_ac:max(), model.test_ac:max()))
	end]]--

end


function computeFeatureMaps(model_path, cifar_class, topK)

	local topK = topK or 10
	local cifar_class = cifar_class or 10

	torch.setdefaulttensortype('torch.FloatTensor')
	require 'xlua'

	model_opt = {}
	model_opt.batchSize = 64

	if cifar_class == 100 then 
		print '=== A. LOAD DATA ==='
		dofile 'load_data_cifar100.lua'
		trainData, testData = load_data_cifar100()
	else
		print '=== A. LOAD DATA ==='
		dofile 'load_data_cifar10.lua'
		trainData, testData = load_data_cifar10()		
	end

	print '=== B. PREPROCESS DATA ==='
	dofile 'preprocess_data_cifar.lua'
	trainData, testData = preprocess_data_cifar(trainData, testData)

	local model     = torch.load(model_path)

	local classTopTables = {}

	for class = 1, testData.label:max() do

		print('Finding l2-norm top feature maps for class ' .. class)

		local l = trainData.label:float()

		local useData = l:eq(class)
		local useInds = torch.range(1,l:numel())[useData]

		-- subselect the data
		local labels = trainData.label[useData]
		local images = trainData.data:index(1,useInds:long())

		--netTable = l2table(model, images,labels)

		local topTables = {}

		-- Calculate top l2 maps in batches
		ti = 1
		for t = 1, labels:numel(), model_opt.batchSize do

		    -- disp progress
	        xlua.progress(ti, math.ceil(labels:numel() / model_opt.batchSize))
	        ti = ti + 1 

		    -- create mini batch like for the training set
		    -- TODO A.2
		    local batch_start = t
		    local batch_end   = math.min(t + model_opt.batchSize - 1,  labels:numel())
		    local batch_img = images[{{batch_start, batch_end}}]
		    local batch_lbl = labels[{{batch_start, batch_end}}]


			local numImages = batch_lbl:numel()

			model:forward(batch_img)

			for k,v in pairs(model:findModules('nn.SpatialConvolution')) do

				-- x contains the feature maps for this image
				local x = v.output
				local lastDim = v.output:nDimension()
				local numFeatureMaps = x:size()[lastDim - 2]

				--calculate the l2norm and reshape into column vector
				--torch.norm(x, p, dim)
				x = x:pow(2):sum(lastDim-1):sum(lastDim):sqrt():squeeze()
				x = x:reshape(x:numel())

				-- lookup column for the images
				local imgIdcol = useInds[{{batch_start ,batch_end}}]
						:repeatTensor(1,numFeatureMaps)
						:reshape(numFeatureMaps,numImages):t():reshape(numImages*numFeatureMaps)

				-- compute columns for feature map ID (number)
				local featureIdCol = torch.range(1, numFeatureMaps):typeAs(x):repeatTensor(numImages)

				-- merge columns into table for this layer
				local layerTable = x:cat(featureIdCol,2):cat(imgIdcol,2)

				-- add to table containing results of all layers 
				-- (store as float to avoid overuse of GPU memory)
				if topTables[k] then
					topTables[k] = torch.cat(topTables[k], layerTable, 1):float()
				else
					topTables[k] = layerTable:float()
				end

				 -- sort and only store topK
			     _, sortInd   = topTables[k][{{},1}]:sort(true)
			     topTables[k] = topTables[k]:index(1,sortInd[{{1,topK}}])

			end

		end

		classTopTables[class] = topTables

	end

	return classTopTables
end


function plotFeatureMaps(model, topTables)
	require 'image'
	local l = 1

	for k,v in pairs(model:listModules()) do
	    if torch.typename(v) == 'nn.SpatialConvolution' then
	        local filterIDs = topTables[l][{{},2}]
	        local imageIDs  = topTables[l][{{},3}]
	        local topK      = imageIDs:numel()

	        local images = trainData.data:index(1, imageIDs:long())
	        model:forward(images:float())
	        local layer = model:listModules()[k]

	        local featureMaps = nil

	        for i = 1, topK do
	            featureMap = layer.output[{i,filterIDs[i],}]:reshape(1,layer.output:size(3),layer.output:size(4))
	            featureMap = image.scale(featureMap, 32, 32)
	            if featureMaps then 
	                featureMaps = featureMaps:cat(featureMap, 1)
	            else
	                featureMaps = featureMap
	            end
	        end

	        --print("======== Layer conv " .. l .. " ========")
	        --print(topTables[l])
	        itorch.image(images, {nrow=32})
	        itorch.image(featureMaps, {nrow=32}) 

	        l = l + 1
	    end

	end
end

--classTopTables = computeFeatureMaps('data/alexnet_cifar100B3-1.net', 100)
--torch.save('data/featureMaps-100b3-1.t7', classTopTables)

--computeFeatureMaps('data/alexnet_cifar10A1.net')


--analyse_model('M01')

--compare_models({'A9.01','A9.02','A9.03'}) ]]--