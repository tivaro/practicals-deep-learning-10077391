require 'torch'
require 'xlua'
require 'optim'
local c = require 'trepl.colorize'

local criterion = nn.CrossEntropyCriterion()
if usingCuda then criterion:cuda() end -- HERE WE DEFINE THE OPERATIONS TO BE PERFORMED ON THE GPU
-- Other optimization methods:
-- ASGD (Good for very large datasets and training over multiple machines)
-- CG (Compute full gradients, no mini-batch updates)
-- LBFGS (Compute full gradients, no mini-batch updates)
-- For more options: http://optim.readthedocs.org/en/latest/

function init_loggers(opt, model_opt)
    trainLogger = optim.Logger(paths.concat(opt.save, model_opt.model_name .. '_train.log'))
    testLogger = optim.Logger(paths.concat(opt.save, model_opt.model_name .. '_test.log'))
end

function saveModel(model)
    local filename = paths.concat(opt.save, model_opt.model_name .. '.net')
    os.execute('mkdir -p ' .. sys.dirname(filename))
    print('==> saving model to '..filename)

    --store as float so that we can load the model without cuda, store cleared in order to to save space
    -- we clone the model so that the parameters of the original model remain linked.
    model:clearState()
    model:float()
    torch.save(filename, saveModel)

    -- restore to cuda and restore link to paramters (ulinked because of the float casting)
    if usingCUDA then
        model:cuda()
        parameters, gradParameters = model:getParameters()
    end

end

-- test function
function test(model, model_opt, testData)

    -- local vars
    local time = sys.clock()

    local confusion = optim.ConfusionMatrix(model_opt.nclasses)

    -- averaged param use?
    if average then
        cachedparams = parameters:clone()
        parameters:copy(average)
    end

    -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
    model:evaluate()

    -- test over test data
    local ti = 1
    local ftotal = 0
    for t = 1, testData:size(), model_opt.batchSize do
        -- disp progress
        -- xlua.progress(t, trainData:size())
        xlua.progress(ti, math.ceil(testData:size() / model_opt.batchSize))
        ti = ti + 1 

        -- create mini batch like for the training set
        -- TODO A.2
        local batch_start = t
        local batch_end   = math.min(t + model_opt.batchSize - 1,  testData:size())
        local inputs      = testData.data[{{batch_start, batch_end}}]
        local actual_batch_size = inputs:size()[1]
        local targets     = testData.label[{{batch_start, batch_end}}]
        
        
        -- range is not implemented in CUDA yet
        if usingCUDA then torch.setdefaulttensortype('torch.LongTensor') end 
        local indices = torch.range(batch_start, batch_end)
        if usingCUDA then torch.setdefaulttensortype('torch.CudaTensor') end
            
        if usingCUDA then torch.setdefaulttensortype('torch.CudaTensor')
            targets     = torch.CudaTensor(actual_batch_size) -- HERE WE CONVERT THE DATA TO A CUDA TENSOR
            targets:copy(testData.label:index(1, indices))
        end

        -- test sample
        local preds = model:forward(inputs)
        local f = criterion:forward(preds, targets)
        ftotal = ftotal + f
        confusion:batchAdd(preds, targets)
        -- confusion:add(preds, targets)
    end

   -- timing
   time = sys.clock() - time
   time = time / testData:size()
   print("\n==> time to test 1 sample = " .. (time*1000) .. 'ms')

   -- print confusion matrix
   confusion:updateValids()
   -- print(confusion)
   print(('Test accuracy: '..c.cyan'%.2f'..' %%\t time: %.2f ms'):format(confusion.totalValid * 100, time*1000))

   -- update log/plot
   testLogger:add{['accuracy'] = confusion.totalValid * 100,
                    ['loss']   = ftotal / testData:size(),
                  ['epoch']    = epoch}
   if opt.plot then
      testLogger:style{['accuracy'] = '-'}
      testLogger:plot()
   end

   -- averaged param use?
   if average then
      -- restore parameters
      parameters:copy(cachedparams)
   end
    
   
   -- next iteration:
   confusion:zero()
end


function train(model, opt, model_opt, trainData, testData)

    ----------------------------------------------------------------------
    print '==> Setting up general stuff'
    -- This matrix records the current confusion across classes
    local confusion = optim.ConfusionMatrix(model_opt.nclasses)
    -- Initialize the loggers that are going to record the performance
    init_loggers(opt, model_opt)

    ----------------------------------------------------------------------
    print '==> Setting up the model before training'
    parameters, gradParameters = model:getParameters()

    ----------------------------------------------------------------------
    print '==> Setting up the optimizer and loss'
    optimState = {
      learningRate      = model_opt.learningRate / model_opt.batchSize,
      learningRates     = model_opt.learningRates,
      weightDecay       = model_opt.weightDecay,
      momentum          = model_opt.momentum,
    }

    local optimize  = optim.sgd
    ----------------------------------------------------------------------
    print '==> Setting up the training module which will perform the per epoch training'

    function train_epoch(epoch)

        -- TODO A.4: ADD AN IF STATEMENT TO CHECK IF THE EPOCH HAS ARRIVED FOR
        -- CHANGING THE LEARNING RATE. IF YES, CHANGE THE LEARNING RATE ACCORDINGLY
        print(optimState)
        if model_opt.learningRate_schedule and ((epoch % model_opt.learningRate_schedule.step) == 0) then
            optimState.learningRate = optimState.learningRate / model_opt.learningRate_schedule.factor
            print('Changing learning rate to ' .. optimState.learningRate)
        end
    
        -- local vars
        local time = sys.clock()
        -- set model to training mode
        -- this is import for modules that differ in training and testing, like Dropout
        model:training()
        -- shuffle at each epoch (not implemented in CUDA yet)
        if usingCUDA then torch.setdefaulttensortype('torch.LongTensor') end
        local shuffle = torch.randperm(trainData:size()):long()
        if usingCUDA then torch.setdefaulttensortype('torch.CudaTensor') end

        -- train one epoch
        print('==> doing epoch on training data:')
        print("==> online epoch # " .. epoch .. ' [batchSize = ' .. model_opt.batchSize .. ']')
        local ti = 1
        local ftotal = 0
        for t = 1, trainData:size(), model_opt.batchSize do
            -- disp progress
            -- xlua.progress(t, trainData:size())
            xlua.progress(ti, math.ceil(trainData:size() / model_opt.batchSize))
            ti = ti + 1 

            -- TODO A.2 CREATE MINI BATCH
            local batch_start = t
            local batch_end   = math.min(t + model_opt.batchSize - 1, trainData:size())
            local indices     = shuffle[{{batch_start, batch_end}}]
            local inputs      = trainData.data:index(1, indices)
            local actual_batch_size = inputs:size()[1]
            local targets     = trainData.label:index(1, indices)

            if usingCUDA then
                targets     = torch.CudaTensor(actual_batch_size) -- HERE WE CONVERT THE DATA TO A CUDA TENSOR
                targets:copy(trainData.label:index(1, indices))
            end

            -- implement feval function that evaluates f(X) and df/dX
            local feval = function(theta)
                -- get new parameters
                if theta ~= parameters then parameters:copy(theta) end
                gradParameters:zero() -- reset gradients
                local outputs = model:forward(inputs)
                local f       = criterion:forward(outputs, targets)
                local df_do   = criterion:backward(outputs, targets)
                model:backward(inputs, df_do)
                ftotal = ftotal + f

                confusion:batchAdd(outputs, targets)
                return f, gradParameters
            end


            -- optimize on current mini-batch
            optimize(feval, parameters, optimState)

        end

        time = sys.clock() - time
        time = time / trainData:size()
        print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')

        confusion:updateValids()
        print(('Epoch: ' .. c.cyan'%u) Train loss: ' .. c.cyan'%.2f ' .. ' | Train accuracy: '..c.cyan'%.2f'..' %% ' .. ' | Time: %.2f ms'):format(epoch, ftotal, confusion.totalValid * 100, time*1000))

        -- update logger/plot
        trainLogger:add{['accuracy'] = confusion.totalValid * 100,
                        ['loss']     = ftotal / trainData:size(),
                        ['epoch']    = epoch}


        -- save/log current net (every epoch if no step is specified)
        if (not model_opt.save_step) or ((epoch % model_opt.save_step) == 0) then
            saveModel(model)
        end
        

        -- next epoch
        confusion:zero()
    end

    -- use epoch offset for models that continue on earlier models
    local epoch_offset = model_opt['epoch_offset'] or 0

    for epoch = 1 + epoch_offset, opt.max_epoch + epoch_offset do
        train_epoch(epoch)
        test(model, model_opt, testData)
    end

    saveModel(model)

end