-- First try CUDA
local ok, cunn = pcall(require, 'cunn')
local ok2, cutorch = pcall(require, 'cutorch')
if not ok then
	print('package cunn not found!')
else
	if not ok2 then
			print('package cutorch not found!')
		else
			print('using CUDA')
			torch.setdefaulttensortype('torch.CudaTensor')
			usingCUDA = true
		end
end


-- Othewise try CLtorch
if not (ok and ok2) then
	local ok, clnn = pcall(require, 'clnn')
	local ok2, ctorch = pcall(require, 'cltorch')
	if not ok then
		print('package clnn not found!')
	else
		if not ok2 then
				print('package cltorch not found!')
			else
				print('using OpenCL')
				torch.setdefaulttensortype('torch.FloatTensor')
				cltorch.setDevice(1)
				usingOpenCL = true
			end
	end
end

if not usingCUDA and not usingOpenCL then torch.setdefaulttensortype('torch.FloatTensor') end