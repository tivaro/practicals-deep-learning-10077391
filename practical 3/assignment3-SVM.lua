require 'torch'

------------------------------------------------------
print '=== I. ENVIRONMENT PARAMETERS ==='
-- We will need the GPU implementation for this experiment.
-- We define the default tensor to be of CUDA type.
dofile 'setGPUtensor.lua'
torch.setnumthreads(1)
torch.manualSeed(1)

------------------------------------------------------
print '=== II. EXPERIMENT OPTIONS ==='
opt                 = {}
opt['save']         = 'data/'
opt['max_epoch']    = 300

------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt                   = {}
model_opt['batchSize']      = 128
model_opt['learningRate']   = 1.0
model_opt['weightDecay']    = 0.0005
model_opt['momentum']       = 0.9
model_opt['model_name']     = 'alexnet_egavves'
model_opt['save_step']      = 50

 --on every step iterations, the learning rate is divided by the factor
model_opt['learningRate_schedule'] = {}
model_opt.learningRate_schedule['step']   = 50
model_opt.learningRate_schedule['factor'] = 10 

------------------------------------------------------
print '=== A. LOAD DATA ==='
dofile 'load_data_cifar100.lua'
trainData, testData = load_data_cifar100()

------------------------------------------------------
print '=== B. PREPROCESS DATA ==='
dofile 'preprocess_data_cifar.lua'
trainData, testData = preprocess_data_cifar(trainData, testData)

model = torch.load(opt['save'] .. model_opt['model_name'] .. '.net')

print(model)

dofile 'setup_model.lua'

layers = {'conv5', 'relu5', 'pool5', 'linear6', 'relu6', 'linear7', 'relu7', 'linear8'}

print('Computing features for the test data')
testFeatures = extract_features(model, testData.data, layers)

print('Saving the features')
for _, id in pairs(layers) do
	local tData = {}
	local filename = paths.concat(opt.save, 'testFeatures-' .. id ..  '.t7')

	tData['label'] = testData.label
	tData['data']  = testFeatures[id]

	torch.save(filename, tData)

end
testFeatures = nil

print('Computing features for the train data')
trainFeatures = extract_features(model, trainData.data, layers)

print('Saving the features')
for _, id in pairs(layers) do
	local tData = {}
	local filename = paths.concat(opt.save, 'trainFeatures-' .. id ..  '.t7')

	tData['label'] = trainData.label
	tData['data']  = trainFeatures[id]

	torch.save(filename, tData)

end
trainFeatures = nil
