require 'torch'
require 'image'
require 'nn'

-- IMPLEMENT THE PREPROCESSING FUNCTION FOR YOUR DATA.
-- FOR THE PREPROCESSING EACH CHANNEL SHOULD HAVE MEAN=0 AND ST DEV=1
-- YOU CAN REUSE THE CODE FROM THE 1ST LAB ASSIGNMENT

function preprocess_data_cifar(trData, tsData, useNbatches)
   
   print '==> Normalize data to uniform distribution.'

   channels = {'r', 'g', 'b'}

   mean = {}
   std = {}

   --cast to float
   trData.data = trainData.data:float()
   tsData.data = testData.data:float()

   for i,channel in ipairs(channels) do
      -- Normalize each channel globally:
      mean[i] = trData.data[{ {},i,{},{} }]:mean()
      std[i] = trData.data[{ {},i,{},{} }]:std()
      trData.data[{ {},i,{},{} }]:add(-mean[i])
      trData.data[{ {},i,{},{} }]:div(std[i])
   end

   -- Normalize test data, using the training means/stds
   for i,channel in ipairs(channels) do
      -- normalize each channel globally:
      tsData.data[{ {},i,{},{} }]:add(-mean[i])
      tsData.data[{ {},i,{},{} }]:div(std[i])
   end

   ------------------------------------------------------
   print '==> Verifying the statistics of the data'

   -- It's always good practice to verify that data is properly
   -- normalized. Better be safe than sorry!

   for i,channel in ipairs(channels) do
      local trainMean = trData.data[{ {},i,{},{} }]:mean()
      local trainStd = trData.data[{ {},i,{},{} }]:std()
    
      local testMean = tsData.data[{ {},i,{},{} }]:mean()
      local testStd = tsData.data[{ {},i,{},{} }]:std()
    
      print('training data, '..channel..'-channel, mean: ' .. trainMean)
      print('training data, '..channel..'-channel, standard deviation: ' .. trainStd)
    
      print('test data, '..channel..'-channel, mean: ' .. testMean)
      print('test data, '..channel..'-channel, standard deviation: ' .. testStd)
   end

   -- make sure labels start at 1 rather then 0
   if trData.label:min() == 0 then trData.label:add(1) end
   if tsData.label:min() == 0 then tsData.label:add(1) end

   -- add size functions
   function trData.size(self) return self.label:size()[1] end
   function tsData.size(self) return self.label:size()[1] end


   -- use a limited portion of the data (for debugging)
   if useNbatches then
      Ntr = math.min(trData:size(), useNbatches * model_opt.batchSize)
      Nts = math.min(tsData:size(), useNbatches * model_opt.batchSize)

      trData.label = trData.label[{{1,Ntr},}]
      trData.data  = trData.data[{{1,Ntr},}]
      tsData.label = tsData.label[{{1,Nts},}]
      tsData.data  = tsData.data[{{1,Nts},}]
   end

   return trData, tsData
end
