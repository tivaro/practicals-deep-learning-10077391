#!/bin/bash
#SBATCH -p gpu
#SBATCH -N 2
#SBATCH -t 5-00:00
module load cuda
module load torch7
CUDA_VISIBLE_DEVICES=0 srun -u th "assignment3-cifar100.lua" --model vgg_bn_drop
