Plot = require 'itorch.Plot'
require 'torch'
require 'optim'
require 'load_csv'

function load_model(model_id, data_folder)
	-- get the paths
	local data_folder = data_folder or 'data/'
	local model_dir = paths.concat(data_folder, model_id)

	local model = {}

	-- load the serialised data
	model['model_opts'] = torch.load(paths.concat(model_dir, 'model.opts'))
	model['train_conf'] = torch.load(paths.concat(model_dir, 'train_conf.mat'))
	model['test_conf']  = torch.load(paths.concat(model_dir, 'test_conf.mat'))

	--load the log data
	local losses   = load_csv(paths.concat(model_dir, 'loss.log'))
	local train_ac = load_csv(paths.concat(model_dir, 'train.log'))
	local test_ac  = load_csv(paths.concat(model_dir, 'test.log'))
	

	-- Process the log data
	model['train_loss'] = losses[{{},1}]
	model['test_loss']  = losses[{{},2}]
	model['train_ac'] = train_ac:squeeze()
	model['test_ac']  = test_ac:squeeze()

	-- Create the X Range
	model['epochs'] = train_ac:numel()
	model['xrange'] = torch.linspace(1, model.epochs, model.epochs)

	model.model_opts['model_lbl'] = model.model_opts['model_lbl'] or model.model_opts['model_id']

	return model

end

function analyse_model(model_id)

	local model = load_model(model_id)

	-- Spit everything out
	print('Analysis of model ' .. model_id .. ':')
	print('Model parameters:')
	print(model.model_opts)

	Plot()
	:line(model.xrange, model.train_loss, 'green','training')
	:line(model.xrange, model.test_loss, 'orange','test')
	:title('Loss graph ' .. model_id)
	:yaxis('Loss')
	:xaxis('epochs')
	:legend(true)
	:draw()

	Plot()
	:line(model.xrange, model.train_ac, 'green','training')
	:line(model.xrange, model.test_ac, 'orange','test')
	:title('Mean Accuracy graph ' .. model_id)
	:yaxis('Mean Accuracy')
	:xaxis('epochs')
	:legend(true)
	:draw()

	print('Confusion Matrices:')
	print('Train-set:')
	print(model.train_conf:__tostring__())

	print('Test-set:')
	print(model.test_conf:__tostring__())
	--image.display(train_conf:render()) 

end

function compare_models(model_ids, plot_loss)

	local plot_loss = plot_loss or false

	--load the models
	local models = {}
	for i,model_id in ipairs(model_ids) do
		models[i] = load_model(model_id)
	end

	local colors = {'lightgreen','darkgreen','lightblue','darkblue','red','darkred','orange','yellow','purple','pink'}


	local plot = Plot()
	for i,model in ipairs(models) do
		plot:line(model.xrange, model.train_ac, colors[2*i-1], model.model_opts.model_lbl .. ' training')
		:line(model.xrange, model.test_ac, colors[2*i], model.model_opts.model_lbl .. ' test')
	end
	plot:title('Mean Accuracy')
	:yaxis('Mean Accuracy')
	:xaxis('epochs')
	:legend(true)
	:draw()

	if plot_loss then

		plot = Plot()
		for i,model in ipairs(models) do
			local max_loss = math.max(model.train_loss:max(),model.test_loss:max())

			plot:line(model.xrange, torch.div(model.train_loss, max_loss), colors[2*i-1], model.model_opts.model_lbl .. ' [train]')
			:line(model.xrange, torch.div(model.test_loss, max_loss), colors[2*i], model.model_opts.model_lbl .. ' [test]')
		end
		plot:title('Loss')
		:yaxis('Loss (normalised per model)')
		:xaxis('epochs')
		:legend(true)
		:draw()
	end

	-- Display performance in a table
	local maxLblSize = 10
	for i,model in ipairs(models) do
		maxLblSize = math.max(maxLblSize, #model.model_opts.model_lbl)
	end
	print(string.format("% " .. maxLblSize .. "s |  Train  | Test     |", 'Model'))
	print(string.format("% " .. maxLblSize .. "s-+---------+----------+", '---------'))
	for i,model in ipairs(models) do
		print(string.format("% " .. maxLblSize .. "s |%3.3f %% | %3.3f %% |", model.model_opts.model_lbl, model.train_ac:max(), model.test_ac:max()))
	end

end

--analyse_model('M01')

--compare_models({'A9.01','A9.02','A9.03'})