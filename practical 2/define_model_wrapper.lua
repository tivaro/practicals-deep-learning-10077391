require 'nn'
require 'MyModules/MyRequ'
--require 'MyModules/MySin'

function define_model_wrapper(D, nhidden, C)
  ------------------------------------------------------------------------------
  -- MODEL
  ------------------------------------------------------------------------------
  
  -- OUR MODEL:
  --     linear -> sigmoid/requ -> linear -> softmax

  model = nn.Sequential()
  model:add(nn.Reshape(D))
  model:add(nn.Linear(D, nhidden))
  model:add(nn.MyRequ())
  model:add(nn.Linear(nhidden, C))
  model:add(nn.LogSoftMax())

  -- Practically copy paste your code from the script file

  ------------------------------------------------------------------------------
  -- LOSS FUNCTION
  ------------------------------------------------------------------------------

  -- Practically copy paste your code from the script file

  criterion = nn.ClassNLLCriterion()

  return model, criterion
end

return define_model_wrapper

