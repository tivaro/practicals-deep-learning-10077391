function load_csv(filename, delimiter, skip_first_row)
	local delimiter = delimiter or '\t'
	local skip_first_row = skip_first_row or true

	-- The dimensions of the tensor we are about to fill
	local nLines = 0
	local nCols  = 0
	local data

	csvFile = io.open(filename, 'r')  


	if skip_first_row then
		header = csvFile:read()
		nLines = -1
	end
	
	-- Determine the number of lines
	for _ in io.lines(filename) do
		nLines = nLines + 1
	end


	local i = 0  
	for line in csvFile:lines('*l') do  
		i = i + 1

		local l = line:split(delimiter)

		-- On the first iteration, determine the number of collumns and preallocate the tensor.
		if i == 1 then
			nCols = #l
			data = torch.Tensor(nLines, nCols)
		end

		-- Now fill the tensor column by columns
		for col, val in ipairs(l) do
			data[i][col] = val
		end
	end

	csvFile:close()  

	return data

end