require 'torch'
require 'nn'
require 'image'
require 'MyModules/MyRequ'
-- require 'MyModules/MyDropout'

------------------------------------------------------

print '==> Define dimensionalities, etc.'

-- Input and output dimensionality
D       = trainData.data:size()[2] * trainData.data:size()[3] * trainData.data:size()[4]
C       = #classes


letters = {'A','B','C','D','E','F','G'}

experiments = {}

function add_experiment()
	local experiment = {}
	experiment['opt'] = opt
	experiment['model_opt'] = model_opt
	experiment['model'] = model
	table.insert(experiments, experiment)
end


--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                 MODEL A8.01                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--
opt = default_opts()
model_opt = default_model_opts()

model_opt['model_id']	  = 'A8.01'
model_opt['optim_name'] = 'optim.sgd'
model_opt['loss_name']  = opt.loss:__tostring()
model_opt['model_description']	  = 'Default model as required for question A.8'
model_opt['model_lbl'] = 'default model'

-- Define the model
model = nn.Sequential()
model:add(nn.Reshape(D))
model:add(nn.Linear(D, model_opt.nhidden))
model:add(nn.Tanh())
model:add(nn.Linear(model_opt.nhidden, C))
model:add(nn.LogSoftMax())

--add_experiment()

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                 MODEL A9.01                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--
opt = default_opts()
model_opt = default_model_opts()

model_opt['model_id']	  = 'A9.01'
model_opt['optim_name'] = 'optim.sgd'
model_opt['loss_name']  = opt.loss:__tostring()
model_opt['model_description']	  = 'Deepened model with Tanh modules as required for question A.9'
model_opt['model_lbl'] = 'Tanh'

-- Define the model
model = nn.Sequential()
model:add(nn.Reshape(D))
model:add(nn.Linear(D, model_opt.nhidden))
model:add(nn.Tanh())
model:add(nn.Linear(model_opt.nhidden, C))
model:add(nn.Tanh())
model:add(nn.LogSoftMax())

--add_experiment()

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                 MODEL A9.02                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--
opt = default_opts()
model_opt = default_model_opts()

model_opt['model_id']	  = 'A9.02'
model_opt['optim_name'] = 'optim.sgd'
model_opt['loss_name']  = opt.loss:__tostring()
model_opt['model_description']	  = 'Deepened model with sigmoid modules as required for question A.9'
model_opt['model_lbl'] = 'sigmoid'

-- Define the model
model = nn.Sequential()
model:add(nn.Reshape(D))
model:add(nn.Linear(D, model_opt.nhidden))
model:add(nn.Sigmoid())
model:add(nn.Linear(model_opt.nhidden, C))
model:add(nn.Sigmoid())
model:add(nn.LogSoftMax())

--add_experiment()

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                 MODEL A9.03                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--
opt = default_opts()
model_opt = default_model_opts()

model_opt['model_id']	  = 'A9.03'
model_opt['optim_name'] = 'optim.sgd'
model_opt['loss_name']  = opt.loss:__tostring()
model_opt['model_description']	  = 'Deepened model with ReLu modules as required for question A.9'
model_opt['model_lbl'] = 'ReLu'

-- Define the model
model = nn.Sequential()
model:add(nn.Reshape(D))
model:add(nn.Linear(D, model_opt.nhidden))
model:add(nn.ReLU())
model:add(nn.Linear(model_opt.nhidden, C))
model:add(nn.ReLU())
model:add(nn.LogSoftMax())

--add_experiment()

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--             MODELS A9.04A-C                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--


for i, nhidden in ipairs({100, 200, 400}) do
	opt = default_opts()
	model_opt = default_model_opts()

	model_opt['model_id']	  = 'A9.04' .. letters[i]
	model_opt['optim_name'] = 'optim.sgd'
	model_opt['loss_name']  = opt.loss:__tostring()
	model_opt['model_description']	  = 'Deepened model with one TanH and one ReLu module, and ' .. nhidden ..' hidden units as required for question A.9'
	model_opt['model_lbl'] = 'TanH & ReLu (n=' .. nhidden .. ')'
	model_opt['nhidden'] = nhidden

	-- Define the model
	model = nn.Sequential()
	model:add(nn.Reshape(D))
	model:add(nn.Linear(D, model_opt.nhidden))
	model:add(nn.Tanh())
	model:add(nn.Linear(model_opt.nhidden, C))
	model:add(nn.ReLU())
	model:add(nn.LogSoftMax())

	--add_experiment()

end

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                MODEL A10.1-2                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--

for i, loss in ipairs({nn.MultiMarginCriterion(), nn.ClassNLLCriterion()}) do
	opt = default_opts()
	model_opt = default_model_opts()

	opt['loss'] = loss

	model_opt['model_id']	  = 'A10.' .. i
	model_opt['optim_name'] = 'optim.sgd'
	model_opt['loss_name']  = opt.loss:__tostring()
	model_opt['model_description']	  = 'Best model so far with ' .. model_opt['loss_name'] .. ' Loss as required for question A.10'
	model_opt['nhidden'] = 400
	model_opt['model_lbl'] = model_opt['loss_name']

	-- Define the model
	model = nn.Sequential()
	model:add(nn.Reshape(D))
	model:add(nn.Linear(D, model_opt.nhidden))
	model:add(nn.Tanh())
	model:add(nn.Linear(model_opt.nhidden, C))
	model:add(nn.ReLU())
	model:add(nn.LogSoftMax())

	--add_experiment()
end

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                MODEL A11.1                   --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--

optimizations = {
	{func=optim.sgd, name='SGD'},
	{func=optim.asgd, name='ASGD'},
	{func=optim.adam, name='ADAM'},
}

for i, optimization in ipairs(optimizations) do
	opt = default_opts()
	model_opt = default_model_opts()

	opt['optimization'] = optimization.func

	model_opt['model_id']	  = 'A11.' .. i
	model_opt['optim_name'] = 'optim.' .. optimization.name
	model_opt['loss_name']  = opt.loss:__tostring()
	model_opt['model_description']	  = 'Best model so far with ' .. optimization.name .. ' optimization as required for question A.11'
	model_opt['nhidden']   = 400
	model_opt['maxEpochs'] = 25
	model_opt['model_lbl'] = optimization.name

	-- Define the model
	model = nn.Sequential()
	model:add(nn.Reshape(D))
	model:add(nn.Linear(D, model_opt.nhidden))
	model:add(nn.Tanh())
	model:add(nn.Linear(model_opt.nhidden, C))
	model:add(nn.ReLU())
	model:add(nn.LogSoftMax())

	--add_experiment()
end

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--               MODELS A12.1                   --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--


for i, weightDecay in ipairs({0, 0.01, 0.1, 0.5, 1}) do
	opt = default_opts()
	model_opt = default_model_opts()

	model_opt['weightDecay'] = weightDecay

	model_opt['model_id']	  = 'A12.1' .. letters[i]
	model_opt['optim_name'] = 'optim.sgd'
	model_opt['loss_name']  = opt.loss:__tostring()
	model_opt['model_description']	  = 'Best model so far with weight decay = ' .. weightDecay
	model_opt['nhidden']   = 400
	model_opt['maxEpochs'] = 25
	model_opt['model_lbl'] = 'Wdecay=' .. weightDecay

	-- Define the model
	model = nn.Sequential()
	model:add(nn.Reshape(D))
	model:add(nn.Linear(D, model_opt.nhidden))
	model:add(nn.Tanh())
	model:add(nn.Linear(model_opt.nhidden, C))
	model:add(nn.ReLU())
	model:add(nn.LogSoftMax())

	--add_experiment()
end

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--               MODELS A12.2                   --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--


for i, momentum in ipairs({0, 0.5, 1, 2}) do
	opt = default_opts()
	model_opt = default_model_opts()

	model_opt['momentum'] = momentum

	model_opt['model_id']	  = 'A12.2' .. letters[i]
	model_opt['optim_name'] = 'optim.sgd'
	model_opt['loss_name']  = opt.loss:__tostring()
	model_opt['model_description']	  = 'Best model so far with momentum = ' .. momentum
	model_opt['nhidden']   = 400
	model_opt['maxEpochs'] = 25
	model_opt['model_lbl'] = 'momentum=' .. momentum

	-- Define the model
	model = nn.Sequential()
	model:add(nn.Reshape(D))
	model:add(nn.Linear(D, model_opt.nhidden))
	model:add(nn.Tanh())
	model:add(nn.Linear(model_opt.nhidden, C))
	model:add(nn.ReLU())
	model:add(nn.LogSoftMax())

	--add_experiment()
end

--==============--==============--==============--==============--==============--==============--==============--
--==============--==============--                 MODEL B5.01                  --==============--==============--
--==============--==============--==============--==============--==============--==============--==============--
opt = default_opts()
model_opt = default_model_opts()

model_opt['model_id']	  = 'B5.01'
model_opt['optim_name'] = 'optim.sgd'
model_opt['loss_name']  = opt.loss:__tostring()
model_opt['model_description']	  = 'Default model using rectified quadratic units'
model_opt['model_lbl'] = 'MyRequ'

-- Define the model
model = nn.Sequential()
model:add(nn.Reshape(D))
model:add(nn.Linear(D, model_opt.nhidden))
model:add(nn.MyRequ())
model:add(nn.Linear(model_opt.nhidden, C))
model:add(nn.LogSoftMax())

add_experiment()