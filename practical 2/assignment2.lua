----------------------------------------------------------------------
-- Section 2.1
----------------------------------------------------------------------
require 'torch'
require 'optim'
require 'nn'

------------------------------------------------------
print '=== I. ENVIRONMENT PARAMETERS ==='
torch.setdefaulttensortype('torch.FloatTensor')
torch.setnumthreads(1)
torch.manualSeed(1)

------------------------------------------------------
print '=== II. EXPERIMENT OPTIONS ==='
function default_opts()

	local opt           = {}
	opt['loss']         = nn.ClassNLLCriterion()
	-- Other loss choices:
	-- criterion = 
	-- nn.ClassNLLCriterion()
	-- nn.MultiMarginCriterion()
	-- For classification: ClassNLLCriterion
	-- For regression: MSECriterion
	-- For more options: https://github.com/torch/nn/blob/master/doc/criterion.md
	opt['save']         = 'data/'
	opt['optimization'] = optim.sgd
	-- Other optimization methods:
	-- ASGD (Good for very large datasets and training over multiple machines)
	-- CG (Compute full gradients, no mini-batch updates)
	-- LBFGS (Compute full gradients, no mini-batch updates)
	-- For more options: http://optim.readthedocs.org/en/latest/
	opt['small']        = {} -- {10000, 5000}

	return opt
end


------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
function default_model_opts()
	local model_opt           = {}
	model_opt['model_id']	  = 'undefined'
	model_opt['nhidden']      = 100
	model_opt['batchSize']    = 1
	model_opt['learningRate'] = 1e-3
	model_opt['weightDecay']  = 0.0
	model_opt['momentum']     = 0.0
	model_opt['maxEpochs']    = 50

	--Store the used functions as string so that we know which we used above
	model_opt['optim_name'] = ''
	model_opt['loss_name']  = ''
	model_opt['model_description']	  = ''
	model_opt['model_lbl'] = nill

	return model_opt
end

----------------------------------------------------------------------
-- Section 2.1
----------------------------------------------------------------------
------------------------------------------------------
print '=== A. LOAD DATA ==='
dofile 'load_data_mnist.lua'

------------------------------------------------------
print '=== B. LOAD DATA ==='
dofile 'preprocess_data_mnist.lua'

------------------------------------------------------

print '=== C. DEFINE EXPERIMENTS ==='
dofile 'define_experiments.lua'



for i, experiment in ipairs(experiments) do

	print '===================================='
	print('===== RUNNING EXPERIMENT ' .. i .. '/' .. #experiments .. ' =====')
	print '===================================='

	print '=== D1. DEFINE MODEL ==='
	opt 	  = experiment.opt
	model_opt = experiment.model_opt
	model 	  = experiment.model

	------------------------------------------------------
	print '==> This is the model:'
	print(model)

	------------------------------------------------------
	print '==> Define loss'
	criterion = opt.loss

	----------------------------------------------------------------------
	print '==> This is the loss function:'
	print(criterion)


	------------------------------------------------------
	print '=== D2. DEFINE TRAINING ==='
	dofile 'define_training.lua'

	------------------------------------------------------
	print '=== D3. DEFINE TESTING ==='
	dofile 'define_testing.lua'

	----------------------------------------------------------------------
	print '=== RUN THE EXPERIMENT ==='

	-- epoch tracker
	epoch = 1
	model_opt.maxEpochs = model_opt.maxEpochs or math.huge

	while epoch <= model_opt.maxEpochs do
	   train()
	   test()

	   --log the losses
	   local train_loss = criterion:forward(model:forward(trainData.data), trainData.labels:float())
	   local test_loss  = criterion:forward(model:forward(testData.data), testData.labels:float())
	   lossLogger:add{['train'] = train_loss, ['test'] = test_loss}

	   epoch = epoch + 1
	end

end